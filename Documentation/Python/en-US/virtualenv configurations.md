# Installing virtualenv
pip install virtualenv

## Installing virtualenv with a specific python version
pip<version_number> install virtualenv

# Creating virtualenv environment
virtualenv <virtualenv_name>

# Creating virtualenv environment with a specific python version
python<version_number> -m virtualenv <virtualenv_name>

# Activate virtualenv environment
## Windows
<virtualenv_name>\Scripts\activate
## Linux and macOS
source <virtualenv_name>/bin/activate

# Deactivate virtualenv environment
deactivate