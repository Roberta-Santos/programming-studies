# How to Work With Multiple Versions of Python Without Conflict

Note: These tests were performed on Windows 10.

# Problem to be solved

I needed several versions of Python running without conflicts on the same machine to be able to work.
Sometimes, in some large projects, each component is running on a different version of python, so it is necessary to run a certain version without conflict with the others.
Having a python2x version and a python3x version, it is not that complicated.
Having the paths of the python and python / Scripts folders of each version in the path variable, generally, when executing only python and the desired command, the machine uses python2 by default.
And for python3?
Just run python3.
And when are there two versions of python3 installed?

## Looking for solutions

When running the python3 command, my machine only displayed version 3.8.2, and I was unable to see or use version 3.7.7, even with the paths configured in the path variable.

### Creating .bat files

I created a .bat file for each version of python:
python37.bat
python38.bat
Within each file, I put the corresponding python path.
I tested it by placing the files in the windows and system32 folder.
The result was the same in both, it didn't work.
Every time I ran, a window was opened in the terminal, in the folder where the .bat files were, where I couldn't execute the python commands in the project folder I was working on.

### Renaming python executables

In the folder for each version of python, there is a file called python.exe.
What if I renamed this file to python37.exe?
It worked in part. I could already see the python version when executing each command, python 37 and python38.
But when running other commands, an error occurred when calling other python files.

## Solution

What if I created a copy of the executable?
The idea was to keep the original executable, python.exe, but put another file in the folder, this one called python37.exe and python 38.exe, respectively.
When executing the commands in the terminal, python37 and python38, yes, it worked perfectly.
I created a virtualenv with each version of python and tested the version inside it, and the version was correct.

Note: It is not necessary to do the same with the pip. Inside the python / Scripts folder, there is already a file called pip3.7 and pip3.8, respectively.