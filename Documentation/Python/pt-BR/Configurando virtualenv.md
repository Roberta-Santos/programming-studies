# Instalando a virtualenv
pip install virtualenv

## Instalando a virtualenv com uma versão específica do python
pip<version_number> install virtualenv

# Criando o ambiente com virtualenv
virtualenv <virtualenv_name>

# Criando o ambiente com virtualenv com uma versão específica do python
python<version_number> -m virtualenv <virtualenv_name>

# Ativando o ambiente com virtualenv
## Windows
<virtualenv_name>\Scripts\activate
## Linux e macOS
source <virtualenv_name>/bin/activate

# Desativando o ambiente com virtualenv
deactivate