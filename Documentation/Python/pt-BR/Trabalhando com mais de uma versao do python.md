# Como Trabalhar Com Várias Versões do Python Sem Conflitos

Observação: Esses testes foram realizados no Windows 10.

# Problema a ser resolvido

Eu precisava de várias versões do Python rodando sem conflitos na mesma máquina para poder trabalhar.
Às vezes, em alguns grandes projetos, cada componente está rodando em uma versão diferente do python, então é preciso executar uma determinada versão sem conflito com as demais.
Tendo uma versão python2x e outra versão python3x, não é tão complicado.
Tendo os caminhos da pasta do python e python/Scripts de cada versão na variável path, geralmente, ao executar apenas python e o comando pretendido, a máquina usa o python2 por padrão.
E para o python3?
Basta rodar python3.
E quando existem duas versões do python3 instaladas?

## Procurando soluções

Ao executar o comando python3, minha máquina exibia apenas a versão 3.8.2, e eu não conseguia ver nem utilizar a versão 3.7.7, mesmo com os caminhos configurados na variável path.

### Criando arquivos .bat

Criei um arquivo .bat para cada versão do python:
python37.bat
python38.bat
Dentro de cada arquivo, coloquei o caminho do python correspondente.
Testei colocando os arquivos na pasta windows e system32.
O resultado foi o mesmo em ambas, não funcionou.
Toda vez que eu executava, uma janela era aberta no terminal, na pasta em que estavam os arquivos .bat, onde eu não conseguia executar os comandos do python na pasta do projeto que eu estava trabalhando.

### Renomeando os executáveis do python

Na pasta de cada versão do python, existe um arquivo chamado python.exe.
E se eu renomeasse esse arquivo para python37.exe?
Funcionou em parte. Eu já conseguia ver a versão do python ao executar cada comando, python 37 e python38.
Mas ao rodar outros comandos, acontecia um erro ao chamar outros arquivos do python.

## Solução

E se eu criasse uma cópia do executável?
A ideia era manter o executável original, python.exe, mas colocar outro arquivo na pasta, esse sim chamado python37.exe e python 38.exe, respectivamente.
Ao executar os comandos no terminal, python37 e python38, sim, funcionou perfeitamente.
Criei uma virtualenv com cada versão do python e testei a versão dentro dela, e a versão estava correta.

Observação: Não é preciso fazer o mesmo com o pip. Dentro da pasta python/Scripts, já existe um arquivo chamado pip3.7 e pip3.8, respectivamente.