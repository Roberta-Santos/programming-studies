# Code to show the mean, median and quantile
import pandas as pd

df = pd.DataFrame([1, 1, 50, 50, 50, 50, 50, 50, 50, 60, 60, 65, 70, 200, 250, 300, 700])
print(df.mean())
#121.0
print(df.median())
#50.0
print(df.quantile())
#50.0